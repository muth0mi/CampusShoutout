package io.github.coderoly.campusshoutout.authentication;

import android.content.Context;
import android.support.annotation.BoolRes;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.ProviderQueryResult;

import io.github.coderoly.campusshoutout.R;

/**
 * Created by oly on 11/05/17.
 */

public class FirebaseAuthMethods {

    public void RegisterUser(String typedEmail, String typedPassword, final Context context) {

        Toast.makeText(context, "Register me", Toast.LENGTH_SHORT).show();

        FirebaseAuth.getInstance().createUserWithEmailAndPassword(typedEmail, typedPassword)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(context, "Account Created", Toast.LENGTH_SHORT).show();

                            return;
                        } else {
                            Toast.makeText(context, "An error occurred", Toast.LENGTH_SHORT).show();
                            return;
                        }
                    }
                });

    }

    public void LogInUser(String typedEmail, String typedPassword, final Context context) {

        Toast.makeText(context, "Sign Me In", Toast.LENGTH_SHORT).show();

        FirebaseAuth.getInstance().signInWithEmailAndPassword(typedEmail, typedPassword)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(context, "Log In Success", Toast.LENGTH_SHORT).show();

                            return;
                        } else {
                            Toast.makeText(context, "An error occurred", Toast.LENGTH_SHORT).show();

                        }

                    }
                });

    }
}