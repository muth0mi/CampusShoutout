package io.github.coderoly.campusshoutout.slideshow;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import io.github.coderoly.campusshoutout.R;
import io.github.coderoly.campusshoutout.authentication.AuthChooser;

/**
 * Created by oly on 14/05/17.
 */


public class LastFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.slideshow_last, container, false);

        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        v.findViewById(R.id.BtnLastSlideshow).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                 getFragmentManager().beginTransaction().replace(R.id.MainActivityContainer, new AuthChooser()).commit();

                //getFragmentManager().beginTransaction().add(R.id.slideshow_main_content, new AuthChooser()).commit();

            }
        });


        return v;
    }

    public static LastFragment newInstance(String text) {

        LastFragment f = new LastFragment();
        Bundle b = new Bundle();
        b.putString("msg", text);

        f.setArguments(b);

        return f;
    }
}